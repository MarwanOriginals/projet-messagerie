package client;
import java.util.Scanner;
import java.rmi.RemoteException;
import client.testClient;
import org.apache.catalina.User;

import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

import soap.MailServerStub;
import soap.MailServerStub.GetMessages;
import soap.MailServerStub.Message;
import soap.MailServerStub.RemoveMessage;
import soap.MailServerStub.RemoveMessages;
import soap.MailServerStub.RemoveMessagesContainWord;
import soap.MailServerStub.SendMessage;

public class MessagerieClient {

	public static void supprAllMail(String u) {
		try {	
		MailServerStub a = new MailServerStub();
		RemoveMessages r = new RemoveMessages();
		r.setTo(u);

			System.out.println(a.removeMessages(r).get_return());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	public static void supprOneMailWithFilter(String receiver ,String filter) {
		try {	
		MailServerStub a = new MailServerStub();
		RemoveMessagesContainWord r = new RemoveMessagesContainWord();
		r.setWord(filter);
		r.setTo(receiver);

			System.out.println(a.removeMessagesContainWord(r).get_return());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	public static void supprOneMail(String receiver ,String title) {
		try {	
		MailServerStub a = new MailServerStub();
		RemoveMessage r = new RemoveMessage();
		r.setTitle(title);
		r.setTo(receiver);
			System.out.println(a.removeMessage(r).get_return());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	public static void sendMail(String sender, String receiver,  String title, String message) {
		try {	
		MailServerStub a = new MailServerStub();
		SendMessage s = new SendMessage();
		s.setFromp(sender);
		s.setTop(receiver);
		s.setTitlep(title);
		s.setMessagep(message);
		System.out.println(a.sendMessage(s).get_return());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void getMails (String u) {
		try {	
		MailServerStub a = new MailServerStub();
		GetMessages g = new GetMessages();
		
		g.setTo(u);
		Message[] result;
			result = a.getMessages(g).get_return();
			if (result == null) 
			{System.out.println("Pas de message");
			}else {
			for(Message ele : result)
			{
				System.out.println("Objet : "+ele.getTitle());
				System.out.println("De : "+ele.getFrom());
				System.out.println("Le : "+ele.getMessageDate());
				System.out.println("Message : "+ele.getMessage());
				System.out.println("--------------------------------------------");
			}}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public static void accuseReception (String u, String person) {
		try {	
		MailServerStub a = new MailServerStub();
		GetMessages g = new GetMessages();
		
		g.setTo(u);
		Message[] result;
			result = a.getMessages(g).get_return();
			if (result == null) 
			{System.out.println("Pas de message");
			}else {
			for(Message ele : result)
			{
				System.out.println("Objet : "+ele.getTitle());
				System.out.println("De : "+ele.getFrom());
				System.out.println("Le : "+ele.getMessageDate());
				System.out.println("Message : "+ele.getMessage());
				System.out.println("--------------------------------------------");
				if(ele.getFrom().equals(person)) {
					sendMail(u, person, "Accuse de reception", "jai bien recu votre message merci");
				}else {
					supprOneMail(u, ele.getTitle());
				}
			}}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public static void scenario1(String u) {
		getMails(u);
		supprOneMailWithFilter(u, "SPAM");
	}
	public static void scenario2(String u,String person) {
		accuseReception(u, person);
	}
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Bienvenue dans votre messagerie, veuillez vous identifier (1) ou creer un compte (2): ");
		int test = scan.nextInt();
        System.out.println(test);
		String result ="";
	    String currentUser = "";
        switch (test) {

        case 1:

        	do {
        	if(result.equals("user not founded")) {
        		System.out.println("Mauvaise combinaison pseudo/mdp");	        		
        	}
    		System.out.println("Entrez votre identifiant");	
    		String ident = scan.next();
    		System.out.println("Entrez votre mdp:");
    		String mdp = scan.next();
    		result = testClient.testAuth(ident,mdp);
            currentUser = ident;
        	}while(!result.equals("user founded"));
        	
    		System.out.println("Connexion reussie");        	
        	break;
        
        case 2:
        	do {
        	if(result.equals("user already exist")) {
        		System.out.println("Utilisateur existe deja");	        		
        	}
    		System.out.println("Entrez le mail que vous souhaitez creer");	
    		String newIdent = scan.next();
    		System.out.println("Entrez votre mdp:");
    		String newMdp = scan.next();
    		result = testClient.createUser(newIdent,newMdp);
            currentUser = newIdent;
    	}while(!result.equals("user created"));  
    		System.out.println("Utilisateur cree");
    	break;
        }
        int choice = 0;
        do {
		System.out.println("Menu des mail : ");  
		System.out.println("Lire ses mails : 1 "); 
		System.out.println("Ecrire un mail : 2 "); 
		System.out.println("Supprimer ses mails : 3 ");
		System.out.println("Supprimer les spam : 4 ");
		System.out.println("Supprimer un mail: 5 ");
		System.out.println("Scenario 1: 6 ");
		System.out.println("Scenario 2: 7 ");
		System.out.println("Quitter le programme : 8");
		choice = scan.nextInt();  
		switch(choice) {
		case 1:
			getMails(currentUser);
			break;
		case 2:
			scan.nextLine();
			System.out.println("Pour : "); 		
			String user = scan.nextLine();
			System.out.println("Objet : "); 		
			String titre = scan.nextLine();
			System.out.println("Message : "); 		
			String msg = scan.nextLine();
			sendMail(currentUser,user,titre,msg);
			break;
		case 3:
			supprAllMail(currentUser);	
			break;
		case 4:
			scan.nextLine();
			System.out.println("Filtre : "); 
			String filter = scan.nextLine();
			supprOneMailWithFilter(currentUser, filter);
			break;
		case 5:
			scan.nextLine();
			System.out.println("titre du mail a supprimer: "); 
			String title = scan.nextLine();			
			supprOneMail(currentUser, title);
			break;
		case 6:
			scenario1(currentUser);
		case 7:
			scan.nextLine();
			System.out.println("Personne a envoyer acuse de reception : "); 
			String person = scan.nextLine();
			scenario2(currentUser, person);
		}
        }while(choice != 8);
		System.out.println("Au revoir !");   


	}

}
