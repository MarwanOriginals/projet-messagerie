package SOAP;
import java.util.*;

public class MailServer
{
	List<Message> messages;
	public MailServer()
	{
		messages = new ArrayList<Message>();
	}

	public String sendMessage(String fromp, String top, String messagep, String titlep)
	{
		Message toSend = new Message(fromp, top, messagep, 1, new Date(), titlep);
		if (titlep.toLowerCase().contains("urgent"))
			toSend.setPriority(2);
		if (titlep.toLowerCase().contains("spam"))
			toSend.setPriority(0);
		
		DB_Message sendToDB = new DB_Message();
		sendToDB.WriteMessage(toSend);
		
		return ("Message sent successfully");
	}

	public List<Message> getMessages (String to)
	{
		List<Message> messages = new ArrayList<Message>();
		DB_Message getFromDB = new DB_Message();
		messages = getFromDB.getMessages(to);
		return messages;
	}

	public String removeMessages (String to)
	{
		DB_Message deleteFromDB = new DB_Message();
		deleteFromDB.DeleteMessage(to);
		return ("Messages removed successfully");
	}
	
	public String removeMessage(String title, String to)
	{
		DB_Message deleteFromDB = new DB_Message();
		deleteFromDB.DeleteOneMessage(title, to);
		
		return ("One message has been removed successfully");
	}
	
	public String removeMessagesContainWord(String word, String to)
	{
		DB_Message deleteFromDB = new DB_Message();
		deleteFromDB.DeleteMessageContainWord(to, word);
		
		return ("Message that contain a word has been removed successfully");
	}

}