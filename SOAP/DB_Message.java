package SOAP;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import REST.User;

public class DB_Message 
{

	
	public void WriteMessage(Message messageToWrite)
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(  
					"jdbc:mysql://localhost:8889/marwan?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","marwan","2244");
			Statement stmt = con.createStatement();  
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");  
		    String strDate = formatter.format(messageToWrite.getMessageDate());
			
			stmt.executeUpdate("INSERT INTO Message VALUES('" + messageToWrite.getMessage() + "', '" + messageToWrite.getFrom() + 
					"', '" + messageToWrite.getTo() + 
					"', '" + String.valueOf(messageToWrite.getIsNew()) + "', '" + messageToWrite.getPriority() + 
					"', '" + strDate 
					+ "', '" + messageToWrite.getTitle() + "')");
			con.close(); 
			System.out.println("INSERT OK");
		} catch (Exception  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void DeleteMessage(String top)
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(  
					"jdbc:mysql://localhost:8889/marwan?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","marwan","2244");
			Statement stmt = con.createStatement();  
			stmt.executeUpdate("DELETE FROM Message WHERE Message.to = '" + top + "'");
			con.close(); 
			System.out.println("DELETE OK");
		} catch (Exception  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void DeleteOneMessage(String title, String to)
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(  
					"jdbc:mysql://localhost:8889/marwan?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","marwan","2244");
			Statement stmt = con.createStatement();  
			stmt.executeUpdate("DELETE FROM Message WHERE Message.title = '" + title + "' AND Message.to = '" + to + "'");
			con.close(); 
			System.out.println("DELETE OK");
		} catch (Exception  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void DeleteMessageContainWord(String to, String word)
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(  
					"jdbc:mysql://localhost:8889/marwan?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","marwan","2244");
			Statement stmt = con.createStatement();  
			stmt.executeUpdate("DELETE FROM Message WHERE Message.title LIKE '%" + word + "%' AND Message.to = '" + to + "'");
			con.close(); 
			System.out.println("DELETE WITH FILTER : " + word);
		} catch (Exception  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<Message> getMessages(String top)
	{
		List<Message> result = new ArrayList<Message>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(  
					"jdbc:mysql://localhost:8889/marwan?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","marwan","2244");
			
			Statement stmt = con.createStatement();  
			ResultSet rs = stmt.executeQuery("select * from Message where Message.to = '" + top + "'");
			while(rs.next())  
			{
				result.add(new Message(rs.getString("from"), rs.getString("to"), rs.getString("messageContent"), rs.getInt("priority"), rs.getDate("messageDate"), rs.getString("title")));
			}
			con.close(); 
			System.out.println("READ OK");
		} catch (Exception  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public List<User> getUsers()
	{
		List<User> result = new ArrayList<User>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(  
					"jdbc:mysql://localhost:8889/marwan?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","marwan","2244");
			
			Statement stmt = con.createStatement();  
			ResultSet rs = stmt.executeQuery("select * from User");
			while(rs.next())  
			{
				result.add(new User(rs.getString("Mail"), rs.getString("Password")));
			}
			con.close(); 
			System.out.println("READ OK");
		} catch (Exception  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public void createUser(User newUser)
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(  
					"jdbc:mysql://localhost:8889/marwan?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","marwan","2244");
			
			Statement stmt = con.createStatement();  
			stmt.executeUpdate("INSERT INTO User VALUES('" + newUser.getMail() + "', '" + newUser.getPassword() + "')");
			con.close(); 
			System.out.println("INSERT USER OK");
		} catch (Exception  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
