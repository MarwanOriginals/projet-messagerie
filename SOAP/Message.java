package SOAP;

import java.util.Date;

public class Message
{
	private String title;
    private String message;
    private String from;
    private String to;
    private boolean isNew;
    private int priority;
    private Date messageDate;
    
    public Message(String from, String to, String message, int priority, Date messageDate, String title)
    {
    	this.message = message;
    	this.from = from;
    	this.to = to;
    	isNew = true;
    	this.priority = priority;
    	this.messageDate = messageDate;
    	this.title = title;
    }
    
    public String getTo()
    {
    	return this.to;
    }
    
    public String getFrom()
    {
    	return this.from;
    }
    
    public String getMessage()
    {
    	return this.message;
    }
    
    public boolean getIsNew()
    {
    	return this.isNew;
    }
    
    public int getPriority()
    {
    	return this.priority;
    }
    
    public Date getMessageDate()
    {
    	return this.messageDate;
    }
    
    public String getTitle()
    {
    	return this.title;
    }
    
    public void setTitle(String title)
    {
    	this.title = title;
    }
    
    public void setMessageDate(Date messageDate)
    {
    	this.messageDate = messageDate;
    }
    
    public void setPriority(int priority)
    {
    	this.priority = priority;
    }
    
    public void setTo(String to)
    {
    	this.to = to;
    }
    
    public void setFrom(String from)
    {
    	this.from = from;
    }
    
    public void setIsNew(boolean isNew)
    {
    	this.isNew = isNew;
    }

}