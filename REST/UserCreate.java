package REST;

import java.util.List;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import SOAP.DB_Message;
public class UserCreate extends ServerResource {
	
	@Get  
	public String toString() {  
		return "Create User";  
	}  
	
	@Post
	public Representation acceptItem(Representation entity) {  
		Representation result = null;  
		// Parse the given representation and retrieve data
		Form form = new Form(entity);  
		String mail = form.getFirstValue("mail");  
		String password = form.getFirstValue("pwd"); 
		
		User user = new User(mail, password);
		DB_Message userDB = new DB_Message();
		List<User> users = userDB.getUsers();
		for(User ele : users)
		{
			if(ele.getMail().equals(user.getMail()))
			{
				result = new StringRepresentation("user already exist",  
			            MediaType.TEXT_PLAIN);
				return result;
			}
		}
		userDB.createUser(user);
		result = new StringRepresentation("user created",  
	            MediaType.TEXT_PLAIN);
		return result;
	}
}
