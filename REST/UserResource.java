package REST;

import java.util.List;

import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.*;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import SOAP.DB_Message;

public class UserResource extends ServerResource {  	
	@Get  
	public String toString() {
		String uid = (String) getRequestAttributes().get("uid");
		return "Information about user \"" + uid + "\" is: <nothing>";  
	}  

	@Post
	public Representation acceptItem(Representation entity) {  
		Representation result = null;  
		// Parse the given representation and retrieve data
		Form form = new Form(entity);  
		String mail = form.getFirstValue("mail");  
		String password = form.getFirstValue("pwd"); 
		
		User user = new User(mail, password);
		DB_Message userDB = new DB_Message();
		List<User> users = userDB.getUsers();
		for(User ele : users)
		{
			if(ele.getMail().equals(user.getMail()) && ele.getPassword().equals(user.getPassword()))
			{
				result = new StringRepresentation("user founded");
				return result;
			}
		}
		result = new StringRepresentation("user not founded");
		return result;
	} 
} 